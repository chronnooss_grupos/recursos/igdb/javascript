const getRandomInt = (min, max) => {
   return Math.floor(Math.random() * (max - min)) + min;
};

console.log(getRandomInt(1, 151));

document.addEventListener("DOMContentLoaded", () => {
   const random = getRandomInt(1, 151);
   fetchData(random);
});

// https://pokeapi.co/docs/v2#pokemon
// https://pokeapi.co/
// https://documenter.getpostman.com/view/10670805/SzS2xToN

const fetchData = async (id) => {
   try {
      $("#tblPokedexGen1 tbody tr").remove();
      var t = "";
      for (var k = 1; k <= 151; k++) {
         var tr = "";
         const res = await fetch("https://pokeapi.co/api/v2/pokemon/" + k);
         const data = await res.json();

         tr += "<tr>";
         tr += "<td class='align-middle'>" + data.id + "</td>";
         // tr +=
         //    "<td class='align-middle'> <div class='row'> <img style='width: 100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" +
         //    k +
         //    ".png></div></td>";
         // tr +=
         //    "<td class='align-middle'> <div class='row'> <img style='width: 100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/" +
         //    k +
         //    ".png></div></td>";
         tr +=
            "<td class='align-middle'> <div class='row'> <img style='width:100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/" +
            k +
            ".png></div></td>";
         tr += "<td class='align-middle'>" + data.name + "</td>";
         tr += "<td class='align-middle'>";
         $.each(data.types, function (i, item) {
            tr +=
               "<span class='badge bg-" +
               item.type.name +
               " mx-1'>" +
               item.type.name +
               " </span>";
         });
         tr += "</td>";
         tr += "<td class='align-middle'>" + data.stats[0].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[1].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[2].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[3].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[4].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[5].base_stat + "</td>";
         tr += "</tr>";
         t += tr;
      }
      document.getElementById("tbltblPokedexGen1Body").innerHTML += t;
   } catch (error) {
      console.log(error);
   }

   try {
      $("#tblPokedexGen2 tbody tr").remove();
      var t = "";
      for (var k = 152; k <= 251; k++) {
         var tr = "";
         const res = await fetch("https://pokeapi.co/api/v2/pokemon/" + k);
         const data = await res.json();

         tr += "<tr>";
         tr += "<td class='align-middle'>" + data.id + "</td>";
         // tr +=
         //    "<td class='align-middle'> <div class='row'> <img style='width: 100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" +
         //    k +
         //    ".png></div></td>";
         // tr +=
         //    "<td class='align-middle'> <div class='row'> <img style='width: 100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/" +
         //    k +
         //    ".png></div></td>";
         tr +=
            "<td class='align-middle'> <div class='row'> <img style='width:100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/" +
            k +
            ".png></div></td>";
         tr += "<td class='align-middle'>" + data.name + "</td>";
         tr += "<td class='align-middle'>";
         $.each(data.types, function (i, item) {
            tr +=
               "<span class='badge bg-" +
               item.type.name +
               " mx-1'>" +
               item.type.name +
               " </span>";
         });
         tr += "</td>";
         tr += "<td class='align-middle'>" + data.stats[0].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[1].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[2].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[3].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[4].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[5].base_stat + "</td>";
         tr += "</tr>";
         t += tr;
      }
      document.getElementById("tbltblPokedexGen2Body").innerHTML += t;
   } catch (error) {
      console.log(error);
   }

   try {
      $("#tblPokedexGen3 tbody tr").remove();
      var t = "";
      for (var k = 252; k <= 386; k++) {
         var tr = "";
         const res = await fetch("https://pokeapi.co/api/v2/pokemon/" + k);
         const data = await res.json();

         tr += "<tr>";
         tr += "<td class='align-middle'>" + data.id + "</td>";
         // tr +=
         //    "<td class='align-middle'> <div class='row'> <img style='width: 100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" +
         //    k +
         //    ".png></div></td>";
         // tr +=
         //    "<td class='align-middle'> <div class='row'> <img style='width: 100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/" +
         //    k +
         //    ".png></div></td>";
         tr +=
            "<td class='align-middle'> <div class='row'> <img style='width:100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/" +
            k +
            ".png></div></td>";
         tr += "<td class='align-middle'>" + data.name + "</td>";
         tr += "<td class='align-middle'>";
         $.each(data.types, function (i, item) {
            tr +=
               "<span class='badge bg-" +
               item.type.name +
               " mx-1'>" +
               item.type.name +
               " </span>";
         });
         tr += "</td>";
         tr += "<td class='align-middle'>" + data.stats[0].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[1].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[2].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[3].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[4].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[5].base_stat + "</td>";
         tr += "</tr>";
         t += tr;
      }
      document.getElementById("tbltblPokedexGen3Body").innerHTML += t;
   } catch (error) {
      console.log(error);
   }

   try {
      $("#tblPokedexGen4 tbody tr").remove();
      var t = "";
      for (var k = 387; k <= 494; k++) {
         var tr = "";
         const res = await fetch("https://pokeapi.co/api/v2/pokemon/" + k);
         const data = await res.json();

         tr += "<tr>";
         tr += "<td class='align-middle'>" + data.id + "</td>";
         // tr +=
         //    "<td class='align-middle'> <div class='row'> <img style='width: 100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" +
         //    k +
         //    ".png></div></td>";
         // tr +=
         //    "<td class='align-middle'> <div class='row'> <img style='width: 100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/" +
         //    k +
         //    ".png></div></td>";
         tr +=
            "<td class='align-middle'> <div class='row'> <img style='width:100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/" +
            k +
            ".png></div></td>";
         tr += "<td class='align-middle'>" + data.name + "</td>";
         tr += "<td class='align-middle'>";
         $.each(data.types, function (i, item) {
            tr +=
               "<span class='badge bg-" +
               item.type.name +
               " mx-1'>" +
               item.type.name +
               " </span>";
         });
         tr += "</td>";
         tr += "<td class='align-middle'>" + data.stats[0].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[1].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[2].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[3].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[4].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[5].base_stat + "</td>";
         tr += "</tr>";
         t += tr;
      }
      document.getElementById("tbltblPokedexGen4Body").innerHTML += t;
   } catch (error) {
      console.log(error);
   }

   try {
      $("#tblPokedexGen5 tbody tr").remove();
      var t = "";
      for (var k = 495; k <= 649; k++) {
         var tr = "";
         const res = await fetch("https://pokeapi.co/api/v2/pokemon/" + k);
         const data = await res.json();

         tr += "<tr>";
         tr += "<td class='align-middle'>" + data.id + "</td>";
         // tr +=
         //    "<td class='align-middle'> <div class='row'> <img style='width: 100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" +
         //    k +
         //    ".png></div></td>";
         // tr +=
         //    "<td class='align-middle'> <div class='row'> <img style='width: 100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/" +
         //    k +
         //    ".png></div></td>";
         tr +=
            "<td class='align-middle'> <div class='row'> <img style='width:100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/" +
            k +
            ".png></div></td>";
         tr += "<td class='align-middle'>" + data.name + "</td>";
         tr += "<td class='align-middle'>";
         $.each(data.types, function (i, item) {
            tr +=
               "<span class='badge bg-" +
               item.type.name +
               " mx-1'>" +
               item.type.name +
               " </span>";
         });
         tr += "</td>";
         tr += "<td class='align-middle'>" + data.stats[0].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[1].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[2].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[3].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[4].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[5].base_stat + "</td>";
         tr += "</tr>";
         t += tr;
      }
      document.getElementById("tbltblPokedexGen5Body").innerHTML += t;
   } catch (error) {
      console.log(error);
   }

   try {
      $("#tblPokedexGen6 tbody tr").remove();
      var t = "";
      for (var k = 650; k <= 721; k++) {
         var tr = "";
         const res = await fetch("https://pokeapi.co/api/v2/pokemon/" + k);
         const data = await res.json();

         tr += "<tr>";
         tr += "<td class='align-middle'>" + data.id + "</td>";
         // tr +=
         //    "<td class='align-middle'> <div class='row'> <img style='width: 100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" +
         //    k +
         //    ".png></div></td>";
         // tr +=
         //    "<td class='align-middle'> <div class='row'> <img style='width: 100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/" +
         //    k +
         //    ".png></div></td>";
         tr +=
            "<td class='align-middle'> <div class='row'> <img style='width:100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/" +
            k +
            ".png></div></td>";
         tr += "<td class='align-middle'>" + data.name + "</td>";
         tr += "<td class='align-middle'>";
         $.each(data.types, function (i, item) {
            tr +=
               "<span class='badge bg-" +
               item.type.name +
               " mx-1'>" +
               item.type.name +
               " </span>";
         });
         tr += "</td>";
         tr += "<td class='align-middle'>" + data.stats[0].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[1].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[2].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[3].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[4].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[5].base_stat + "</td>";
         tr += "</tr>";
         t += tr;
      }
      document.getElementById("tbltblPokedexGen6Body").innerHTML += t;
   } catch (error) {
      console.log(error);
   }

   try {
      $("#tblPokedexGen7 tbody tr").remove();
      var t = "";
      for (var k = 722; k <= 809; k++) {
         var tr = "";
         const res = await fetch("https://pokeapi.co/api/v2/pokemon/" + k);
         const data = await res.json();

         tr += "<tr>";
         tr += "<td class='align-middle'>" + data.id + "</td>";
         // tr +=
         //    "<td class='align-middle'> <div class='row'> <img style='width: 100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" +
         //    k +
         //    ".png></div></td>";
         // tr +=
         //    "<td class='align-middle'> <div class='row'> <img style='width: 100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/" +
         //    k +
         //    ".png></div></td>";
         tr +=
            "<td class='align-middle'> <div class='row'> <img style='width:100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/" +
            k +
            ".png></div></td>";
         tr += "<td class='align-middle'>" + data.name + "</td>";
         tr += "<td class='align-middle'>";
         $.each(data.types, function (i, item) {
            tr +=
               "<span class='badge bg-" +
               item.type.name +
               " mx-1'>" +
               item.type.name +
               " </span>";
         });
         tr += "</td>";
         tr += "<td class='align-middle'>" + data.stats[0].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[1].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[2].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[3].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[4].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[5].base_stat + "</td>";
         tr += "</tr>";
         t += tr;
      }
      document.getElementById("tbltblPokedexGen7Body").innerHTML += t;
   } catch (error) {
      console.log(error);
   }

   try {
      $("#tblPokedexGen8 tbody tr").remove();
      var t = "";
      for (var k = 810; k <= 898; k++) {
         var tr = "";
         const res = await fetch("https://pokeapi.co/api/v2/pokemon/" + k);
         const data = await res.json();

         tr += "<tr>";
         tr += "<td class='align-middle'>" + data.id + "</td>";
         // tr +=
         //    "<td class='align-middle'> <div class='row'> <img style='width: 100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" +
         //    k +
         //    ".png></div></td>";
         // tr +=
         //    "<td class='align-middle'> <div class='row'> <img style='width: 100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/" +
         //    k +
         //    ".png></div></td>";
         tr +=
            "<td class='align-middle'> <div class='row'> <img style='width:100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/" +
            k +
            ".png></div></td>";
         tr += "<td class='align-middle'>" + data.name + "</td>";
         tr += "<td class='align-middle'>";
         $.each(data.types, function (i, item) {
            tr +=
               "<span class='badge bg-" +
               item.type.name +
               " mx-1'>" +
               item.type.name +
               " </span>";
         });
         tr += "</td>";
         tr += "<td class='align-middle'>" + data.stats[0].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[1].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[2].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[3].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[4].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[5].base_stat + "</td>";
         tr += "</tr>";
         t += tr;
      }
      document.getElementById("tbltblPokedexGen8Body").innerHTML += t;
   } catch (error) {
      console.log(error);
   }
};
function obtPokedexPorGeneracion() {
   $("#formCatalogoPokemon").submit((e) => {
      e.preventDefault();

      var busqueda = $("#txtBusquedaPokemon").val();
      var generacion = $("#selGeneraciones option:selected").val();

      var min = 0;
      var max = 0;

      switch (generacion) {
         case 1:
            min = 1;
            max = 151;
            break;
         case 2:
            min = 152;
            max = 251;
            break;
         case 3:
            min = 252;
            max = 386;
            break;
         case 4:
            min = 387;
            max = 494;
            break;
         case 5:
            min = 495;
            max = 649;
            break;
         case 6:
            min = 650;
            max = 721;
            break;
         case 7:
            min = 722;
            max = 809;
            break;
         case 8:
            min = 810;
            max = 898;
            break;
         default:
            min = 1;
            max = 898;
      }

      try {
         $("#tblPokedexGen tbody tr").remove();
         var t = "";
         for (var k = min; k <= max; k++) {
            var tr = "";
            const res = await fetch("https://pokeapi.co/api/v2/pokemon/" + k);
            const data = await res.json();

            tr += "<tr>";
            tr += "<td class='align-middle'>" + data.id + "</td>";
            // tr +=
            //    "<td class='align-middle'> <div class='row'> <img style='width: 100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" +
            //    k +
            //    ".png></div></td>";
            // tr +=
            //    "<td class='align-middle'> <div class='row'> <img style='width: 100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/" +
            //    k +
            //    ".png></div></td>";
            tr +=
               "<td class='align-middle'> <div class='row'> <img style='width:100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/" +
               k +
               ".png></div></td>";
            tr += "<td class='align-middle'>" + data.name + "</td>";
            tr += "<td class='align-middle'>";
            $.each(data.types, function (i, item) {
               tr +=
                  "<span class='badge bg-" +
                  item.type.name +
                  " mx-1'>" +
                  item.type.name +
                  " </span>";
            });
            tr += "</td>";
            tr +=
               "<td class='align-middle'>" + data.stats[0].base_stat + "</td>";
            tr +=
               "<td class='align-middle'>" + data.stats[1].base_stat + "</td>";
            tr +=
               "<td class='align-middle'>" + data.stats[2].base_stat + "</td>";
            tr +=
               "<td class='align-middle'>" + data.stats[3].base_stat + "</td>";
            tr +=
               "<td class='align-middle'>" + data.stats[4].base_stat + "</td>";
            tr +=
               "<td class='align-middle'>" + data.stats[5].base_stat + "</td>";
            tr += "</tr>";
            t += tr;
         }
         document.getElementById("tbltblPokedexGenBody").innerHTML += t;
      } catch (error) {
         console.log(error);
      }
   });
   return false;
}
